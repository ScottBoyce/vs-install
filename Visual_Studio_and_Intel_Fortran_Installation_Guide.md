# Integrated Development Environment (IDE)

This document provides an overview on how to setup the tools for compiling [MODFLOW-OWHM](https://code.usgs.gov/modflow/mf-owhm). In particular, the installation and setup of [MS Visual Studio](https://visualstudio.microsoft.com/) (the IDE) and [Intel oneAPI](https://www.intel.com/content/www/us/en/developer/tools/oneapi/overview.html) (Fortran and C compilers). The tools mentioned are not endorsed in any way, but are used to build the official executable releases of MODFLOW-OWHM. Fortunately, as of writing this document, both offer both Visual Studio and oneAPI offer free, no-cost editions. [GFortran](https://gcc.gnu.org/wiki/GFortran) and [LLVM Flang](https://flang.llvm.org/docs/) are open-source compilers that are available, but come with several limitations. First, GFortran contains several compiler bugs that prevent it from successfully compiling MODFLOW-OWHM. This may change with future releases of GFortran and consequently, the provided *makefile* does include a gfortran option. Flang is a relatively new Fortran compiler that is part of the [LLVM](https://llvm.org/) project. While LLVM does support MS Windows, Flang does not.

This document will first explain what Visual Studio is, and how to do a minimal installation for setting up oneAPI. Then it will go over the minimal installation of oneAPI in order to compile MODFLOW-OWHM.


[[_TOC_]]     


## Visual Studio Community Installation Guide (Windows 10)

Visual Studio (VS) does a major release every two to three years and distinguishes the version with the year it's released. The three recent versions are Visual Studio 2017, Visual Studio 2019, Visual Studio 2022  (denoted as, vs2017, vs2019, vs2022, respectively, and with sematic versioning as v15, v16, v17, respectively). The key difference between the "major" versions (vs2019, vs2022, vsXYZ) is the Microsoft  `.NET` framework and Microsoft `C` compilers included. This does not affect the Intel OneAPI, so long as the version of visual studio is supported by it. 

It is recommended to use the most recent major release of VS that Intel oneAPI supports (ensure both Fortran and C are supported). The following website provides a list of currently supported VS IDEs by Intel oneAPI:  
https://www.intel.com/content/www/us/en/developer/articles/reference-implementation/intel-compilers-compatibility-with-microsoft-visual-studio-and-xcode.html  
As of writing this, the oneAPI Fortran component does NOT support VS2022, so you must use VS2019.

Another caveat is that VS specific settings cannot be exported and imported between VS major versions. That is, you can not take vs2017, export its settings, then import them into vs2019. 

However, the VS *Project Solution* and *Project Files* should work for both vs2017 and vs2019.

### Visual Studio Input Files

The input to VS is composed of a *Project Solution* (`.sln`) and *Project Files* (eg `.vcxproj` and `.vfproj`). 

- The *Project Solution* file specifies the location of the *Project Files*. 
- The *Project Files* contain a listing of the source files that are compiled in the project and compiler options.

VS uses the *Project Solution* file to open the *Project Files* to access the list of known source files for compilation.

### Visual Studio Editions

Visual Studio comes in three editions.

1. [Community](https://visualstudio.microsoft.com/vs/community)
2. [Professional](https://visualstudio.microsoft.com/vs/professional)
3. [Enterprise](https://visualstudio.microsoft.com/vs/enterprise)

The [Community](https://visualstudio.microsoft.com/vs/community) edition is **free** for students, open-source contributors, and individuals. 

The other two versions charge a fee and provide support options and easier file sharing and collaboration.

If you qualify for the community edition you can download the latest edition installation program at:

https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community  
&nbsp; &nbsp; &nbsp; ➢ **[make sure Intel oneAPI supports it](https://www.intel.com/content/www/us/en/developer/articles/reference-implementation/intel-compilers-compatibility-with-microsoft-visual-studio-and-xcode.html)**

and you can download a specific older version, such as VS2019, at:

https://visualstudio.microsoft.com/vs/older-downloads/

### Install Visual Studio 2019

When you run the installation program there will be a set of updates run for the installer. 

The next window will ask you to install the features of VS that you want to use. At a minimum, for using Intel Visual Fortran, you must include the `Desktop development with C++` option. 

See below for the location of that specific option. Next hit `Install`.

![vs2019_install_prompt](./img/vs2019_install_prompt.png)



After installation, VS will ask if you want to log into your Microsoft account. You can skip this step and log in later if you do not want to log in. The advantage to logging into your Microsoft account is that your settings are transferred across multiple computers (such as dark theme and window placement).

The next window will ask you to select a theme. Personally, I just leave this as *General* and select the *Dark* theme. Note that, this may not appear if you signed into your Microsoft account.

![vs2019_install_prompt2](./img/vs2019_install_prompt2.png)





The next window will ask you to start a project or download a git repository. 

At this time, just select `Continue without code`.

![vs2019_install_prompt3](./img/vs2019_install_prompt3.png)



- You have successfully installed Visual Studio.
- Now close the Visual Studio window. 



## Visual Studio Recommended Plugin (Optional)

For ease with using git, it is best to keep all C and Fortran source files with Unix style endings (`LF`) instead of dos style (`CR LF`). 
This minor semantic can make working with git or Linux/Unix machines easier.

To automatically use Unix style ending you can add the Visual Studio Extension [Line Endings Unifier](https://marketplace.visualstudio.com/items?itemName=JakubBielawa.LineEndingsUnifier). 

*Note that you may get a message saying "One or more extensions were loaded using deprecated API's", and you can click "Don't show this message for current extensions" to disable the warning. This warning occurs with the most recent update to Visual Studio and may be fixed by the plugin developer in the future. It does not seem to affect the plugin itself.*

- To install this extension go to either:
  - vs2017: The menu  `Tools` and select  `Extensions and Updates`. 
  - vs2019: The menu  `Extensions` and select  `Manage Extensions`. 
- Select the `Online` option on the upper left of the window and the type into the search bar `Line Endings Unifier`. 
- At that point you should see the following (Note the figure below that shows the Online option and the search bar circled in red). 
- Click `Download` to install the extension.

![Line_Ending_Unifier_Install](./img/Line_Ending_Unifier_Install.png)



- Close Visual Studio.
- Next, the plugin will install and you must press `Modify` to continue.
- Open Visual Studio and go to the menu `Tools` and select  `Options` to set up the Line Endings Unifier. 
  - The following the following settings will ensure that Unix style endings are enforced:

![Line_Ending_Unifier](./img/Line_Ending_Unifier.png)



1. Set the `Default Line Ending` to **Linux**
2. Set the `Force Default Line Ending On Document Save` to **True**.
3. Set the `Supported File Formats` entry in the above window. 
   The following are the extensions that are in the image:
   `.cpp; .c; .h; .hpp; .cs; .js; .vb; .txt; .f; .for; .fpp; .f90; .f95; .f03; .f08; .ftn; .f77`



## Intel Visual Fortran - Intel OneAPI (Windows 10)

Intel Visual Fortran is now part of the [Intel OneAPI](https://software.intel.com/content/www/us/en/develop/tools/oneapi.html) software suite and is free to use. It requires that Visual Studio to be installed beforehand and two Intel specific installation files. The first is the [Intel oneAPI Base Toolkit](https://software.intel.com/content/www/us/en/develop/tools/oneapi/base-toolkit.html) and the second is [Intel oneAPI HPC Toolkit](https://software.intel.com/content/www/us/en/develop/tools/oneapi/hpc-toolkit.html). The latter contains the Fortran compiler, which is dependent on the base toolkit.

1. Download the Intel oneAPI Base Toolkit by selecting the appropriate boxes at 
   https://software.intel.com/content/www/us/en/develop/tools/oneapi/base-toolkit/download.html 
2. Download the Intel oneAPI HPC Toolkit by selecting the appropriate boxes at 
   https://software.intel.com/content/www/us/en/develop/tools/oneapi/hpc-toolkit/download.html



------

The **Installer Type** option determines the download size. 

- The **Online** installer downloads a small installation file that lets the user configure the total download (requires internet during installation). 
- The **Local** installer downloads all features into a single installation file (no internet required by the installer). This version should be used if the Online installer keeps failing to install the files.

------



### BASE Toolkit Installation - Intel oneAPI

1. Run either the online or local installation exe file downloaded and press continue. 
2. Check the tick box with *I accept the terms of the license agreement* (see the black circle below).
3. Click the *Customize* button (see the black box below).

![oneAPI_base_install1](./img/oneAPI_base_install1.png)

The Intel oneAPI HPC Toolkit (Fortran Compiler) only requires the installation of the following components:

1. Intel Distribution for GDB
2. Intel oneAPI DPC++/C++ Compiler
3. Intel oneAPI DPC++ Library
4. Intel oneAPI Threading Building Blocks

The easiest way to do this is to uncheck all the boxes, note that the installer will complain about dependencies, and then click the four boxes that you need to install. 

Feel free to read about the other components and decide if you want to install them, but they are unnecessary to use the Fortran Compiler.

Once you have configured the correct components, the installation window should look something like this:

![oneAPI_base_install2](./img/oneAPI_base_install2.png)

- Now hit the right arrow at the bottom of the window to continue. 
- Note you may get a warning about no Intel Graphics card, just ignore and continue.
- Next, oneAPI will detect if you have installed Visual Studio and offer integration if it is present. 
  If this box is not checked, then you will not be able to use Visual Studio with Intel Fortran to compile code.

![oneAPI_base_install3](./img/oneAPI_base_install3.png)



- Select you consent or do not consent to information being collected about you and hit next.
- Now get coffee or brew some tea, cause its going to take a while to install.
- Once the installation is done, close/exit the installer and restart your computer.



### HPC Toolkit Installation - Intel oneAPI

The HPC Toolkit can only be installed after the BASE toolkit. The first part of the installation is identical to the BASE installation.

1. Run either the online or local installation exe file downloaded and press continue. 
2. Check the tick box with *I accept the terms of the license agreement*.
3. Click the *Customize* button to continue.

The Intel oneAPI HPC Toolkit (Fortran Compiler) only requires the installation of the following components:

1. Intel oneAPI DPC++/C++ Compiler  
   (note this was previously installed in the BASE Toolkit, but you need it checked or it will uninstall it)
2. Intel Fortran Compiler & Intel Fortran Compiler Classic

The easiest way to do this is to uncheck all the boxes, note that the installer will complain about dependencies, and then click the four boxes that you need to install. 

Feel free to read about the other components and decide if you want to install them, but they are unnecessary to use the Fortran Compiler.

The are two Fortran compilers available 

1. Intel Fortran Compiler (Translates Fortran to the Multi-Level Intermediate Representation for the [LLVM Toolchain](https://llvm.org/)) 
2. Intel Fortran Compiler Classic (The compiler you will most likely use; formerly called Intel Parallel Studio)

Once you have configured the correct components, the installation window should look something like this:

![oneAPI_hpc_install1](./img/oneAPI_hpc_install1.png)



- Now hit the right arrow at the bottom of the window to continue. 
- Next, oneAPI will detect if you have installed Visual Studio and offer integration if it is present. 
- If this box is not checked, then you will not be able to use Visual Studio with Intel Fortran to compile code.
- Next decide if you consent or do not consent to information being collected about you and hit next.



![oneAPI_hpc_install2](./img/oneAPI_hpc_install2.png)



- Now get some more coffee or brew some stronger tea, cause its going to take a while to install.

**You now have installed visual studio with Intel Fortran.**



## Opening MODFLOW-OWHM with Visual Studio for Compilation

This section briefly discusses how to open in Visual Studio the MODFLOW-OWHM *Project Solution* (`.sln`) files and start the Intel Fortran compiler to build an executable (`exe`). It is recommended to also review `ide/README.md`, which explains the important files that pertain to compilation and settings that may need to be adjusted to account for different `C` compiler versions.

The input files for compilating MODFLOW-OWHM, called a *Project Solution* (`.sln`) and *Project Files* (eg `.vcxproj` and `.vfproj`), are located in`ide/visual_studio`.  It is recommended to only open one of the two provided *Project Solution* files (`OneWater_Project.sln` and `OneWater_Project.sln`). The difference between the two is if `GMG` is included in the compilation, which requires the Intel `C` compiler. The following is what you might see in `ide/visual_studio`:

![mf-owhm ide/visual_studio folder](./img/mf-owhm_visual_studio_files.png)

At a minimum you should see:

- `GMG.vcxproj`
- `GMG.vcxproj.filters`
- `OneWater_GMG_Project.sln`
- `OneWater_Project.sln`
- `mf-owhm-gmg.vfproj`
- `mf-owhm.vfproj`

and the additional files are created by Visual Studio for any local customizations, such as breakpoints. 


The first time you run `OneWater_GMG_Project.sln` or `OneWater_Project.sln` you will have to associate it with what program to run it with. I recommend picking a single version of Visual Studio rather than the "Selector". You can always change the version with the Windows "Open With" right click option:

![mf-owhm ide/visual_studio folder](./img/mf-owhm_visual_studio_first_open.png)



Before you can compile you need to select the Configuration and Platform. The Platform should always be set to x64 for a 64bit Windows and the the project includes the following configurations:

- Release  
   &nbsp; ➢ `-O2` Maximize Speed optimization with source line numbers embedded  
   &nbsp; ➢ Builds `mf-owhm.exe`
- Debug  
   &nbsp; ➢ Debug model with all compilation and runtime checks (very slow)  
   &nbsp; ➢ Builds `mf-owhm-debug.exe`
- Fast_Debug  
   &nbsp; ➢ Debug model with only important compilation and runtime checks (slow)  
   &nbsp; ➢ Builds `mf-owhm-fast-debug.exe`

The following is where you can change the Configuration and Platform:

![mf-owhm ide/visual_studio folder](./img/mf-owhm_visual_studio_compile1.png)

To compile a new executable, go to the `Build` menu and select `Build Solution` or `Rebuild Solution`. The only difference between the two, is that Rebuild will delete all existing intermediate files before compilation (this forces all new files to be generated).

![mf-owhm ide/visual_studio folder](./img/mf-owhm_visual_studio_compile2.png)
